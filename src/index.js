import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";

import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import ru from "react-intl/locale-data/ru";

// eslint-disable-next-line import/no-extraneous-dependencies
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { HashRouter } from "react-router-dom";
import App from "./App";
import rootReducer from "./rootReducer";
import * as serviceWorker from "./serviceWorker";
import { userLoggedIn } from "./actions/auth";
import { localeSet } from "./actions/locale";

addLocaleData(en);
addLocaleData(ru);

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

if (localStorage.blogJWT) {
  const user = { token: localStorage.blogJWT };
  store.dispatch(userLoggedIn(user));
}

if (localStorage.lang) {
  store.dispatch(localeSet(localStorage.lang));
}

ReactDOM.render(
  <HashRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </HashRouter>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
