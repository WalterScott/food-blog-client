import { POST_DELETED } from '../types';

export default function articles(state = [], action = {}) {
  switch (action.type) {
    case POST_DELETED:
      return [...state, action.post];
    default:
      return state;
  }
}
