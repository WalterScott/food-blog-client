import { SCROLLED_TO } from "../types";

export default function scroll(state = 0, action = {}) {
  switch (action.type) {
    case SCROLLED_TO:
      return action.height;
    default:
      return state;
  }
}
