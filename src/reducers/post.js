import { PAGE_GET_POST } from '../types';

export default function articles(state = {}, action = {}) {
  switch (action.type) {
    case PAGE_GET_POST:
      return action.post;
    default:
      return state;
  }
}
