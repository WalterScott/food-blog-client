import { USER_REGISTERED } from "../types";

export default function user(state = {}, action = {}) {
  switch (action.type) {
    case USER_REGISTERED:
      return action.user;
    default:
      return state;
  }
}
