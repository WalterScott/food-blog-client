import { SET_LOCALE } from '../types';

export default function language(state = { currentLang: 'ru' }, action = {}) {
  switch (action.type) {
    case SET_LOCALE:
      return { currentLang: action.currentLang };
    default:
      return state;
  }
}
