import { PAGE_GET_ARTICLES } from "../types";

export default function articles(
  state = { pagesCount: 1, articles: [] },
  action = {}
) {
  switch (action.type) {
    case PAGE_GET_ARTICLES:
      return {
        pagesCount: action.pagesCount,
        articles: [...state.articles, ...action.articles.reverse()] //[...state.articles, action.articles.reverse()]
      };
    default:
      return state;
  }
}
