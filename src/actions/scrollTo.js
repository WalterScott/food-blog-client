import { SCROLLED_TO } from "../types";

export const scrollTo = height => ({
  type: SCROLLED_TO,
  height
});
