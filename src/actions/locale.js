import { SET_LOCALE } from '../types';

export const localeSet = currentLang => ({
  type: SET_LOCALE,
  currentLang,
});

export const setLocale = lang => dispatch => {
  localStorage.lang = lang;
  dispatch(localeSet(lang));
};
