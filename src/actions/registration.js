import { USER_REGISTERED } from "../types";
import api from "../api";

export const userRegistered = user => ({
  type: USER_REGISTERED,
  user
});

export const registration = credentials => dispatch =>
  api.user.registration(credentials).then(user => {
    dispatch(userRegistered(user));
  });
