import { PAGE_GET_ARTICLES, PAGE_GET_POST, POST_DELETED } from "../types";
import api from "../api";
import { scrollTo } from "./scrollTo";

export const pageGetArticles = data => ({
  type: PAGE_GET_ARTICLES,
  articles: data.articles,
  pagesCount: data.pagesCount
});

export const pageGetPost = post => ({
  type: PAGE_GET_POST,
  post
});

export const postDeleted = post => ({
  type: POST_DELETED,
  post
});

export const getArticles = pageId => dispatch =>
  api.page.getArticles(pageId).then(data => {
    dispatch(pageGetArticles(data));
  });

export const getPost = id => dispatch =>
  api.page.getPost(id).then(post => {
    dispatch(pageGetPost(post));
  });

export const deletePost = id => dispatch =>
  api.page.deletePost(id).then(post => {
    dispatch(postDeleted(post));
  });
