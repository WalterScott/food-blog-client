import React from "react";
import { Switch, Route } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { IntlProvider } from "react-intl";
import MediaQuery from "react-responsive";
import { connect } from "react-redux";

import {
  HomePage,
  LoginPage,
  CreateArticlePage,
  FullArticlePage,
  UpdateArticlePage,
  RegistrationPage
} from "./components/pages";
import Mainbar from "./components/layouts/MainBar";
import Sidebar from "./components/layouts/SideBar";
import messages from "./messages";

const useStyles = makeStyles(() => ({
  paper: {
    textAlign: "left"
  }
}));

// eslint-disable-next-line arrow-parens
const App = ({ language }) => {
  const classes = useStyles();
  return (
    <IntlProvider
      locale={language.currentLang}
      messages={messages[language.currentLang]}
    >
      <div className="App">
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Mainbar />
          </Grid>
          <Grid item xs={12} sm={8} md={9}>
            <Paper className={classes.paper}>
              {" "}
              <Switch>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/login" component={LoginPage} />
                <Route
                  exact
                  path="/create-post"
                  component={CreateArticlePage}
                />
                <Route exact path="/post/:id" component={FullArticlePage} />
                <Route path="/update-post/:id" component={UpdateArticlePage} />
                <Route path="/registration" component={RegistrationPage} />
              </Switch>
            </Paper>
          </Grid>
          <MediaQuery query="(min-device-width: 600px)">
            <Grid item sm={4} md={3}>
              <Paper className={classes.paper}>
                <Sidebar className="sidebar" style={{ position: "fixed" }} />
              </Paper>
            </Grid>
          </MediaQuery>
        </Grid>
      </div>
    </IntlProvider>
  );
};

App.propTypes = {
  language: PropTypes.shape({
    currentLang: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ language }) => ({
  language
});

export default connect(
  mapStateToProps,
  null
)(App);
