import axios from "axios";

export default {
  user: {
    login: credentials =>
      axios.post("/api/auth", { credentials }).then(res => res.data.user),
    registration: credentials =>
      axios
        .post("/api/registration", { credentials })
        .then(res => res.data.user)
  },
  page: {
    getArticles: pageId =>
      axios.get(`/api/articles/?pageId=${pageId}`).then(res => res.data),
    getPost: id => axios.get(`/api/post/${id}`).then(res => res.data.post),
    createArticle: credentials =>
      axios.post("/api/create-post", { credentials }).then(res => res.data),
    deletePost: id =>
      axios.delete(`/api/delete-post/${id}`).then(res => res.data.deletedPost),
    UpdateArticle: credentials =>
      axios.put("/api/update-post", { credentials }).then(res => res.data)
  }
};
