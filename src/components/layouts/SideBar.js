import React from "react";
import { Link } from "react-router-dom";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import HomeIcon from "@material-ui/icons/Home";
import ControlPointIcon from "@material-ui/icons/ControlPoint";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(() => ({
  link: {
    textDecoration: "none",
    color: "black"
  }
}));

const SideBar = ({ isAuthenticated }) => {
  const classes = useStyles();
  return (
    <div>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <List>
            <ListItem>
              <ListItemIcon>
                <HomeIcon />
              </ListItemIcon>
              <ListItemText>
                <Link to="/" className={classes.link}>
                  <FormattedMessage id="sidebar.home" defaultMessage="Home" />
                </Link>
              </ListItemText>
            </ListItem>
            {isAuthenticated ? (
              <ListItem>
                <ListItemIcon>
                  <ControlPointIcon />
                </ListItemIcon>
                <ListItemText>
                  <Link to="/create-post" className={classes.link}>
                    {" "}
                    <FormattedMessage
                      id="create.button"
                      defaultMessage="Create"
                    />
                  </Link>
                </ListItemText>
              </ListItem>
            ) : (
              ""
            )}
            {!isAuthenticated ? (
              <ListItem>
                <ListItemIcon>
                  <LockOpenIcon />
                </ListItemIcon>
                <ListItemText>
                  <Link to="/registration" className={classes.link}>
                    {" "}
                    <FormattedMessage
                      id="button.registration"
                      defaultMessage="Registration"
                    />
                  </Link>
                </ListItemText>
              </ListItem>
            ) : (
              ""
            )}
          </List>
        </Grid>
      </Grid>
    </div>
  );
};

SideBar.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token,
    lang: state.language.currentLang
  };
}

export default connect(
  mapStateToProps,
  null
)(SideBar);
