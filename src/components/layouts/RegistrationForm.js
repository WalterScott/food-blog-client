import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Validator from "validator";
import { InlineError } from "./";
import { FormattedMessage } from "react-intl";

class RegistrationForm extends Component {
  state = {
    data: {
      email: "",
      password: ""
    },
    loading: false,
    errors: {}
  };

  handleChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.type]: e.target.value }
    });
  };

  submit = () => {
    const errors = this.validate(this.state.data);
    this.setState({
      errors
    });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props.submit(this.state.data).catch(error => {
        this.setState({ errors: error.response.data.errors, loading: false });
      });
    }
  };

  validate = data => {
    const errors = {};
    if (!Validator.isEmail(data.email)) errors.email = "Invalid email";
    if (!data.password) errors.password = "Can't be blank";
    return errors;
  };

  render() {
    const { errors, loading } = this.state;
    return (
      <Fragment>
        {!loading ? (
          <form id="form1">
            {errors.global ? (
              <div>
                <InlineError
                  messageId={errors.global.id}
                  defaultMessage={errors.global.text}
                />
              </div>
            ) : (
              ""
            )}
            <TextField
              label={
                <FormattedMessage
                  id="loginform.label.email"
                  defaultMessage="Email"
                />
              }
              type="email"
              placeholder="example@example.com"
              onChange={this.handleChange}
              margin="normal"
            />
            <br />
            {errors.email && (
              <InlineError
                messageId="error.message.email"
                defaultMessage={errors.email}
              />
            )}
            <br />
            <TextField
              label={
                <FormattedMessage
                  id="loginform.label.password"
                  defaultMessage="Password"
                />
              }
              type="password"
              onChange={this.handleChange}
              margin="normal"
              placeholder="Password"
            />
            <br />
            {errors.password && (
              <InlineError
                messageId="error.message.password"
                defaultMessage={errors.password}
              />
            )}
            <br />
            <Button
              variant="contained"
              form="form1"
              color="default"
              onClick={this.submit}
              style={{ marginLeft: "15%", marginBottom: "50px" }}
            >
              <FormattedMessage
                id="button.registration"
                defaultMessage="Sign Up"
              />
            </Button>
          </form>
        ) : (
          <CircularProgress />
        )}
      </Fragment>
    );
  }
}

RegistrationForm.propTypes = {
  submit: PropTypes.func.isRequired
};

export default RegistrationForm;
