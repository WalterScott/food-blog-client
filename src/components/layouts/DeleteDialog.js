import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { InlineError } from './index';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { FormattedMessage } from 'react-intl';
class DeleteDialog extends Component {
  state = {
    input: '',
    errors: {
      incorrectValue: false,
    },
  };

  handleChange = e =>
    this.setState({
      input: e.target.value,
    });

  validate = () => {
    const lang = {
      ru: 'УДАЛИТЬ',
      en: 'DELETE',
    };
    if (this.state.input === lang[this.props.language.currentLang])
      this.props.btnDeletePost();
    else {
      this.setState({
        errors: {
          incorrectValue: true,
        },
      });
    }
  };
  close = () => {
    this.props.open();
    this.setState({
      errors: { incorrectValue: false },
    });
  };

  render() {
    const { errors } = this.state;
    const { openState, open } = this.props;
    return (
      <div>
        <Dialog
          open={openState}
          onClose={() => open()}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            {' '}
            <FormattedMessage
              id="delete.form.title"
              defaultMessage="Delete Article"
            />
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <FormattedMessage
                id="delete.form.message"
                defaultMessage="Write 'DELETE'"
              />
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              fullWidth
              onChange={this.handleChange}
            />
            {errors.incorrectValue && (
              <InlineError
                messageId="error.message.delete"
                defaultMessage="Incorrect introduced value"
              />
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.close} color="primary">
              <FormattedMessage
                id="delete.form.button.cancel"
                defaultMessage="Cancel"
              />
            </Button>
            <Button onClick={this.validate} color="primary">
              <FormattedMessage
                id="delete.form.button.delete"
                defaultMessage="DELETE"
              />
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

DeleteDialog.propTypes = {
  open: PropTypes.func.isRequired,
  btnDeletePost: PropTypes.func.isRequired,
  openState: PropTypes.bool.isRequired,
};

const mapStateToProps = ({ language }) => ({
  language,
});

export default connect(
  mapStateToProps,
  null,
)(DeleteDialog);
