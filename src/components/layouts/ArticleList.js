import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import { Article } from "./";
import { connect } from "react-redux";
import MediaQuery from "react-responsive";
import ControlPointIcon from "@material-ui/icons/ControlPoint";

const useStyles = makeStyles({
  root: {
    listStyleType: "none",
    textAlign: "justify",
    marginRight: "20px"
  },
  createButton: {
    position: "fixed",
    top: "17%",
    left: "81%"
  }
});

// eslint-disable-next-line arrow-parens
const ArticleList = props => {
  const { articlesInf, user } = props;
  const classes = useStyles();
  return (
    <Grid container spacing={1}>
      <Grid item xs={12}>
        <ul className={classes.root}>
          {articlesInf.articles.map(article => (
            <Article articleItem={article} key={article._id} />
          ))}
        </ul>
        <MediaQuery query="(max-device-width: 600px)">
          {user ? (
            <a
              type="button"
              href="/#/create-post"
              className={classes.createButton}
            >
              <ControlPointIcon style={{ fontSize: "50px", color: "blue" }} />
            </a>
          ) : (
            ""
          )}
        </MediaQuery>
      </Grid>
    </Grid>
  );
};

ArticleList.propTypes = {
  articlesInf: PropTypes.shape({
    articles: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        text: PropTypes.string
      })
    ),
    pagesCount: PropTypes.number
  }).isRequired,
  user: PropTypes.bool.isRequired
};

const mapStateToProps = ({ articlesInf, user }) => ({
  articlesInf,
  user: !!user.token
});

export default connect(
  mapStateToProps,
  null
)(ArticleList);
