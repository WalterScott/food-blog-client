import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

const InlineError = ({ messageId, defaultMessage }) => (
  <div style={{ color: '#990000', textAlign: 'left' }}>
    <FormattedMessage id={messageId} defaultMessage={defaultMessage} />
  </div>
);
InlineError.propTypes = {
  messageId: PropTypes.string.isRequired,
  defaultMessage: PropTypes.string.isRequired,
};

export default InlineError;

/*<div style={{ color: '#990000', textAlign: 'left' }}>
{text}</div>*/
