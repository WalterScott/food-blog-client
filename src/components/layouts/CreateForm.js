import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { InlineError } from './';
import CircularProgress from '@material-ui/core/CircularProgress';
import api from '../../api';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

class CreateForm extends Component {
  state = {
    loading: false,
    post: {
      title: '',
      description: '',
      creationDate: '',
    },
    errors: {
      title: '',
      description: '',
    },
  };

  handleChange = name => ({ target: { value } }) => {
    this.setState({
      post: { ...this.state.post, [name]: value },
    });
  };

  submit = () => {
    const errors = this.validate(this.state.post);
    this.setState({
      errors,
    });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      api.page
        .createArticle(this.state.post)
        .catch(error =>
          this.setState({ errors: error.response.data.errors, loading: false }),
        )
        .then(() => {
          this.setState({ loading: false });
          this.props.history.push('/');
        });
    }
  };

  validate = ({ title, description }) => {
    const errors = {};
    if (title.length === 0) errors.title = 'Invalid title';
    if (description.length < 100) errors.description = 'Too short description';
    if (description.length > 1000) errors.description = 'Too long description';
    return errors;
  };

  render() {
    const { errors, loading } = this.state;
    return (
      <div>
        {!loading ? (
          <form
            noValidate
            autoComplete="off"
            style={{ marginLeft: '20px', marginRight: '50px' }}
          >
            <TextField
              label={
                <FormattedMessage
                  id="createform.label.title"
                  defaultMessage="Title"
                />
              }
              value={this.state.title}
              onChange={this.handleChange('title')}
              margin="normal"
            />
            <br />
            {errors.title && (
              <InlineError
                messageId="error.message.title"
                defaultMessage={errors.title}
              />
            )}
            <br />
            <TextField
              label={
                <FormattedMessage
                  id="createform.label.description"
                  defaultMessage="Description"
                />
              }
              fullWidth
              multiline
              rows={10}
              value={this.state.description}
              onChange={this.handleChange('description')}
              margin="normal"
            />
            <br />
            {errors.description && (
              <InlineError
                messageId={
                  errors.description === 'Too short description'
                    ? 'error.message.description.short'
                    : 'error.message.description.long'
                }
                defaultMessage={errors.description}
              />
            )}
            <br />
            <Button
              variant="contained"
              color="primary"
              onClick={this.submit}
              style={{ marginLeft: '40%' }}
            >
              <FormattedMessage id="create.button" defaultMessage="Create" />
            </Button>
          </form>
        ) : (
          <CircularProgress />
        )}
      </div>
    );
  }
}

CreateForm.propTypes = {
  history: PropTypes.shape({}).isRequired,
};

export default CreateForm;
