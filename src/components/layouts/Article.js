import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  time: {
    color: 'grey',
  },
  title: {
    textDecoration: 'none',
    color: 'blue',
  },
  article: {
    marginBottom: '20px',
  },
});

const Article = ({ articleItem }) => {
  const classes = useStyles();
  return (
    <li>
      <div className={classes.article}>
        <Link to={`/post/${articleItem._id}`} className={classes.title}>
          <Typography> {articleItem.title}</Typography>
        </Link>
        <i>
          <Typography className={classes.time}>
            {' '}
            {articleItem.createdDate}
          </Typography>
        </i>
        <Typography> {articleItem.text}</Typography>
      </div>
    </li>
  );
};

Article.defaultProps = {
  articleItem: [],
};

Article.propTypes = {
  articleItem: PropTypes.shape({
    title: PropTypes.string,
    text: PropTypes.string,
  }),
};

export default Article;
