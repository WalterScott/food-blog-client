import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { logout } from '../../actions/auth';
import { setLocale } from '../../actions/locale';

const Mainbar = ({ isAuthenticated, logout, setLocale, lang }) => (
  <AppBar position="static">
    <Toolbar>
      <Typography variant="h6" style={{ color: 'white' }}>
        <Link to="/" style={{ color: 'white', textDecoration: 'none' }}>
          <FormattedMessage id="mainbar.title" defaultMessage="News" />
        </Link>
      </Typography>
      {lang === 'en' ? (
        <Button
          onClick={() => setLocale('ru')}
          style={{ marginLeft: 'auto', marginRight: 0, color: 'white' }}
        >
          RU
        </Button>
      ) : (
        <Button
          onClick={() => setLocale('en')}
          style={{ marginLeft: 'auto', marginRight: 0, color: 'white' }}
        >
          EN
        </Button>
      )}
      {isAuthenticated ? (
        <Button
          color="inherit"
          style={{ marginLeft: 0, marginRight: 0 }}
          onClick={logout}
        >
          <Link to="/" style={{ color: 'white', textDecoration: 'none' }}>
            <FormattedMessage id="button.logout" defaultMessage="Login" />
          </Link>
        </Button>
      ) : (
        <Button color="inherit" style={{ marginLeft: 0, marginRight: 0 }}>
          <Link to="/login" style={{ color: 'white', textDecoration: 'none' }}>
            <FormattedMessage id="button.login" defaultMessage="Logout" />
          </Link>
        </Button>
      )}
    </Toolbar>
  </AppBar>
);

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token,
    lang: state.language.currentLang,
  };
}

Mainbar.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired,
  setLocale: PropTypes.func.isRequired,
  lang: PropTypes.string.isRequired,
};

export default connect(
  mapStateToProps,
  { logout, setLocale },
)(Mainbar);
