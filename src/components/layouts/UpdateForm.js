import React, { Component, Fragment } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { InlineError } from './';
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import api from '../../api';
//import { UpdateArticle } from '../../actions/crudArticles';

class UpdateForm extends Component {
  state = {
    loading: false,
    post: {
      title: '',
      description: '',
      id: '',
    },
    errors: {
      title: '',
      description: '',
    },
  };

  componentDidMount() {
    this.setState({
      post: {
        title: this.props.post.title,
        description: this.props.post.text,
        id: this.props.post._id,
      },
    });
  }

  handleChange = name => ({ target: { value } }) => {
    this.setState({
      post: { ...this.state.post, [name]: value },
    });
  };

  submit = () => {
    const errors = this.validate(this.state.post);
    this.setState({
      errors,
    });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      api.page
        .UpdateArticle(this.state.post)
        .catch(error =>
          this.setState({ errors: error.response.data.errors, loading: false }),
        )
        .then(() => {
          this.setState({ loading: false });
          this.props.history.push('/');
        });
    }
  };

  validate = ({ title, description }) => {
    const errors = {};
    if (title.length === 0) errors.title = 'Invalid title';
    if (description.length < 100) errors.description = 'Too short description';
    if (description.length > 1000) errors.description = 'Too long description';
    return errors;
  };

  render() {
    const { errors, loading } = this.state;
    return (
      <Fragment>
        {!loading ? (
          <form
            noValidate
            autoComplete="off"
            style={{ marginLeft: '20px', marginRight: '50px' }}
          >
            <TextField
              label={
                <FormattedMessage
                  id="update.label.title"
                  defaultMessage="Title"
                />
              }
              value={this.state.post.title}
              onChange={this.handleChange('title')}
              margin="normal"
            />
            <br />
            {errors.title && (
              <InlineError
                messageId="error.message.title"
                defaultMessage={errors.title}
              />
            )}
            <br />
            <TextField
              label={
                <FormattedMessage
                  id="update.label.description"
                  defaultMessage="Description"
                />
              }
              fullWidth
              multiline
              rows={10}
              value={this.state.post.description}
              onChange={this.handleChange('description')}
              margin="normal"
            />
            <br />
            {errors.description && (
              <InlineError
                messageId={
                  errors.description === 'Too short description'
                    ? 'error.message.description.short'
                    : 'error.message.description.long'
                }
                defaultMessage={errors.description}
              />
            )}
            <br />
            <Button
              variant="contained"
              color="primary"
              onClick={this.submit}
              style={{ marginLeft: '40%' }}
            >
              {<FormattedMessage id="update.button" defaultMessage="Update" />}
            </Button>
          </form>
        ) : (
          <CircularProgress />
        )}
      </Fragment>
    );
  }
}

UpdateForm.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ post }) => ({
  post,
});

export default connect(
  mapStateToProps,
  null,
)(UpdateForm);
