import HomePage from "./HomePage";
import LoginPage from "./LoginPage";
import CreateArticlePage from "./CreateArticlePage";
import FullArticlePage from "./FullArticlePage";
import UpdateArticlePage from "./UpdateArticlePage";
import RegistrationPage from "./RegistrationPage";

export {
  HomePage,
  LoginPage,
  CreateArticlePage,
  FullArticlePage,
  UpdateArticlePage,
  RegistrationPage
};
