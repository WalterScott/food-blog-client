import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { CreateForm } from '../layouts';
import Typography from '@material-ui/core/Typography';

// eslint-disable-next-line react/prefer-stateless-function
class CreateArticlePage extends Component {
  render() {
    const { history } = this.props;
    return (
      <div>
        <Grid spacing={1} container direction="row" alignItems="center">
          <Grid item xs={12}>
            <Typography variant="h4" style={{ textAlign: 'center' }}>
              <FormattedMessage
                id="page.title.create"
                defaultMessage="Create artcile Page"
              />
            </Typography>
            <CreateForm history={history} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

CreateArticlePage.propTypes = {
  history: PropTypes.shape({}).isRequired,
};

export default CreateArticlePage;
