import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import { RegistrationForm } from "../layouts/";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registration } from "../../actions/registration";
import { FormattedMessage } from "react-intl";
import Grid from "@material-ui/core/Grid";

class RegistrationPage extends Component {
  submit = data =>
    this.props.registration(data).then(() => this.props.history.push("/"));
  render() {
    return (
      <div>
        <Grid
          spacing={1}
          container
          direction="column"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={12}>
            <Typography variant="h4">
              <FormattedMessage
                id="page.title.registration"
                defaultMessage="Registration Page"
              />
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <RegistrationForm submit={this.submit} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

RegistrationPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  registration: PropTypes.func.isRequired
};

export default connect(
  null,
  { registration }
)(RegistrationPage);
