import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import { LoginForm } from "../layouts/";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { login } from "../../actions/auth";
import { FormattedMessage } from "react-intl";
import Grid from "@material-ui/core/Grid";
class LoginPage extends Component {
  submit = data =>
    this.props.login(data).then(() => this.props.history.push("/"));
  render() {
    return (
      <div>
        <Grid
          spacing={1}
          container
          direction="column"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={12}>
            <Typography variant="h4">
              <FormattedMessage
                id="page.title.login"
                defaultMessage="Login Page"
              />
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <LoginForm submit={this.submit} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

LoginPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  login: PropTypes.func.isRequired
};

export default connect(
  null,
  { login }
)(LoginPage);
