import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { UpdateForm } from '../layouts';

// eslint-disable-next-line react/prefer-stateless-function
class UpdateArticlePage extends Component {
  render() {
    const { history } = this.props;
    return (
      <div>
        <Grid spacing={1} container direction="row" alignItems="center">
          <Grid item xs={12} sm={12} md={12}>
            <Typography variant="h4" style={{ textAlign: 'center' }}>
              <FormattedMessage
                id="page.title.update"
                defaultMessage="Update Page"
                style={{ textAlign: 'center' }}
              />
            </Typography>
          </Grid>
          <Grid item xs={12} sm={12} md={12}>
            <UpdateForm history={history} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

UpdateArticlePage.propTypes = {
  history: PropTypes.shape({}).isRequired,
};

export default UpdateArticlePage;
