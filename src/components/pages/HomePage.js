import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/styles";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";
import { ArticleList } from "../layouts";
import { getArticles } from "../../actions/crudArticles";
import Typography from "@material-ui/core/Typography";

const styles = {
  title: {
    textAlign: "center",
    margin: "5px 0px 0px 0px"
  }
};

// eslint-disable-next-line arrow-parens
class HomePage extends Component {
  state = {
    pageId: 1,
    scrolling: false
  };

  scrollEvent = e => {
    this.handleScroll(e);
  };

  componentDidMount() {
    console.log(this.props.scroll);
    window.scrollTo({
      top: this.props.scroll,
      behavior: "smooth"
    });
    this.props.getArticles(this.state.pageId);
    window.addEventListener("scroll", this.scrollEvent);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.scrollEvent);
  }

  handleScroll = () => {
    const { scrolling, pageId } = this.state;
    const { articlesInf } = this.props;
    if (scrolling) return;
    if (articlesInf.pagesCount <= pageId) return;
    let lastLi = document.querySelector("ul> li:last-child");
    var lastLiOffset = lastLi.offsetTop + lastLi.clientHeight;
    let pageOffset = window.pageYOffset + window.innerHeight;
    let bottomOffset = 50;
    if (pageOffset > lastLiOffset - bottomOffset) {
      this.getMoreArticles();
    }
  };

  getMoreArticles = () => {
    this.setState({
      pageId: this.state.pageId + 1,
      scrolling: true
    });
    this.props.getArticles(this.state.pageId);
    this.setState({
      scrolling: false
    });
  };

  render() {
    const { classes, articles } = this.props;
    return (
      <div>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Typography variant="h4" className={classes.title}>
              {" "}
              <FormattedMessage
                id="page.title.home"
                defaultMessage="Home Page"
              />
            </Typography>
            <ArticleList articles={articles} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

HomePage.propTypes = {
  getArticles: PropTypes.func.isRequired,
  articles: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      text: PropTypes.string
    }).isRequired
  )
};

const mapStateToProps = ({ articlesInf, scroll }) => ({
  articlesInf,
  scroll
});

export default withStyles(styles)(
  connect(
    mapStateToProps,
    { getArticles }
  )(HomePage)
);
