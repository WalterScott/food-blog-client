import React, { Component } from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/styles";
import UndoIcon from "@material-ui/icons/Undo";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import { getPost, deletePost } from "../../actions/crudArticles";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { DeleteDialog } from "../layouts";

import Grid from "@material-ui/core/Grid";

const styles = {
  title: {
    bold: 600,
    textAlign: "center",
    fontSize: "24px"
  },
  date: {
    color: "grey",
    margin: "0px 0px 0px 30px"
  },
  description: {
    textIndent: "1.5em",
    margin: "5px 30px 20px 30px",
    textAlign: "justify"
  },
  button: {
    margin: "5px 0px 15px 30px"
  },
  undo: {
    margin: "0px 5px 0px 3px"
  }
};

class FullArticle extends Component {
  state = {
    id: "",
    open: false
  };
  componentDidMount() {
    const { getPost, match } = this.props;
    getPost(match.params.id);
    this.setState({ id: match.params.id });
  }

  btnDeletePost = () => {
    const { deletePost, match } = this.props;
    deletePost(match.params.id).then(() => this.props.history.push("/"));
  };

  open = () =>
    this.setState({
      open: !this.state.open
    });

  render() {
    let { post, classes, isAuthenticated } = this.props;
    return (
      <div>
        <Grid container spacing={1} alignItems="center">
          <Grid item xs={12}>
            <Button
              href="/"
              variant="contained"
              color="primary"
              className={classes.button}
            >
              <UndoIcon className={classes.undo} />
              <FormattedMessage id="back.button" defaultMessage="Button" />
            </Button>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.title}>{post.title} </Typography>
            <Typography className={classes.description}>
              {post.text}{" "}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <i>
              {" "}
              <Typography className={classes.date}>
                {post.createdDate}{" "}
              </Typography>
            </i>
          </Grid>
          {isAuthenticated ? (
            <Grid item xs={12}>
              <Button
                className={classes.button}
                variant="contained"
                color="primary"
                onClick={this.btnDeleteUpdate}
              >
                <Link
                  to={`/update-post/${this.state.id}`}
                  style={{ color: "white", textDecoration: "none" }}
                >
                  {" "}
                  <FormattedMessage
                    id="update.button"
                    defaultMessage="Update"
                  />
                </Link>
              </Button>
              <Button
                className={classes.button}
                variant="contained"
                color="primary"
                onClick={this.open}
              >
                <FormattedMessage id="delete.button" defaultMessage="Delete" />
              </Button>
              <DeleteDialog
                open={this.open}
                openState={this.state.open}
                btnDeletePost={this.btnDeletePost}
              />
            </Grid>
          ) : (
            ""
          )}
        </Grid>
      </div>
    );
  }
}

FullArticle.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }).isRequired,
  getPost: PropTypes.func.isRequired,
  user: PropTypes.bool
};

const mapStateToProps = ({ post, user }) => ({
  post,
  isAuthenticated: !!user.token
});

export default withStyles(styles)(
  connect(
    mapStateToProps,
    { getPost, deletePost }
  )(FullArticle)
);
