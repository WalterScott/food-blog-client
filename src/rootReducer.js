import { combineReducers } from "redux";
import user from "./reducers/user";
import articles from "./reducers/articles";
import post from "./reducers/post";
import deletedPost from "./reducers/deletedPost";
import language from "./reducers/language";
import registration from "./reducers/registration";
import scroll from "./reducers/scroll";

export default combineReducers({
  user,
  articlesInf: articles,
  post,
  deletedPost,
  language,
  newUserRegistered: registration,
  scroll
});
